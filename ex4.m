w = 0.1;
n = 5;

img = imread('hermann.jpg');
imgDouble = im2double(img);

% Filter matrix
filter = ones(n, n * 3);
for i = 1 : n
    for j = 1 : n
        filter(i, j) = -w;
        filter(i, 3 * n - j + 1) = -w;
    end
end

filter = padarray(filter, n, -w, 'both');

convolution = conv2(img, filter);
% imagesc(convolution);
imshowpair(imgDouble, convolution, 'montage');
colormap gray;