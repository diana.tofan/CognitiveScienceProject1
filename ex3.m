img = imread('MonaLisa.jpg');
imgDouble = im2double(img, 'indexed');
%imagesc(imgDouble);
colormap gray;

w = 0.1;
n = 30;
% Filter matrix
filter = ones(n, n * 3);
for i = 1 : n
    for j = 1 : n
        filter(i, j) = -w;
        filter(i, 3 * n - j + 1) = -w;
    end
end

filter = padarray(filter, n, -w, 'both');

convolution = conv2(img, filter);
binaryImg = imbinarize(imgDouble, 50);

imagesc(binaryImg);
% imshowpair(convolution, binaryImg, 'montage');
colormap gray;
% subplot(2,3,1); imagesc(monaLisaPic); colormap gray
% subplot(2,3,4); imagesc(monaLisaPic); colormap gray
