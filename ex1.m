I = [1 1 1 1 1 0 0 0 0 0];
N = 10;
w = 0.1;
A = [];
A(1) = I(1) - w * I(2);
A(N) = I(N) - w * I(N-1);

for i = 2:(N-1)
    A(i) = I(i) - w * (I(i-1) + I(i+1));
end
    
disp(A)
plot(A)

edgeArray = findEdge(A, 0.9);
disp(edgeArray);

function edges = findEdge(activationCells, threshold)
    len = length(activationCells);
    edges = zeros(1, len);
    for i = 1 : len
        if activationCells(i) >= threshold
            edges(i) = 1;
        end
    end
end
        
        