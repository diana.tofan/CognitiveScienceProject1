I = [0 0 0 1 1 1 2 2 2 3 3 3 4 4 4 5 5 5];
N = 18;
w = 0.1;
A = [];
A(1) = I(1) - w * I(2);
A(N) = I(N) - w * I(N-1);

for i = 2:(N-1)
    A(i) = I(i) - w * (I(i-1) + I(i+1));
end
    
% disp(A)
% plot(A)
% imagesc(A)
% colormap gray

matrix = [-w 1 -w];
convolution = conv(I, matrix);
disp(convolution);

imagesc(convolution);
colormap gray;
